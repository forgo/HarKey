const util = require('../../utils/util.js');
const keyService = require('../../data/keyService.js');
const globalData = getApp().globalData;
Page({
  data: {
    keyModel: {}
  },
  //复制数据
  copyKeyBind: function() {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    const keyModel = this.data.keyModel;

    let content = '钥匙名称：' + keyModel.keyName;
    if (keyModel.keyAccount)
      content += '\n账号：' + keyModel.keyAccount;
    if (keyModel.keyPwd)
      content += '\n密码：' + keyModel.keyPwd;
    if (keyModel.keyPwd2)
      content += '\n密码2：' + keyModel.keyPwd2;
    if (keyModel.keyUrl)
      content += '\nURL：' + keyModel.keyUrl;
    if (keyModel.keyRemark)
      content += '\n备注：' + keyModel.keyRemark;

    util.setClipboardData(content);
  },
  //删除
  deleteKeyBind: function() {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    const keyID = this.data.keyModel.keyID;
    const option = {
      title: '删除这个钥匙',
      content: '你会失去这个钥匙的信息',
      success(res) {
        if (!res.confirm) //用户点击取消了
          return

        let localJsonData = util.getStorageData(globalData.localJsonName);
        const keyArry = localJsonData.key;
        for (var i = 0; i < keyArry.length; i++) {
          if (keyArry[i].keyID != keyID)
            continue;
          localJsonData.key.splice(i, 1);
          localJsonData.time = new Date().getTime();
          break;
        }
        util.setStorageData(globalData.localJsonName, localJsonData);
        if (!globalData.userInfo.isCloudSyn) {
          util.navigateBack();
          return;
        }
        util.setCloudDbData(globalData.cloudJsonName, localJsonData).then(res => {
          util.navigateBack();
        }).catch(err => {
          console.error(err);

          util.showToast('钥匙删除失败!');
        });
      }
    };
    wx.showModal(option);
  },
  //编辑跳转
  editKeyBind: function() {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    const keyModel = this.data.keyModel;
    util.navigateTo('../edit/edit?type=2', keyModel);
  },
  //复制行数据
  copyValueBind: function(event) {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    const content = event.currentTarget.dataset.value;
    util.setClipboardData(content);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      keyModel: {
        keyID: options.keyID
      }
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const keyID = this.data.keyModel.keyID;
    const localJsonData = util.getStorageData(globalData.localJsonName);
    let keyModel = util.firstOrDefault(localJsonData.key, (item) => item.keyID == keyID);
    keyModel = keyService.deKeyData(keyModel, globalData.password) //解密

    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: keyModel.categoryColor,
    });
    this.setData({
      keyModel: keyModel
    });
  }
})